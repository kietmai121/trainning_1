import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Text1223 from '../Screen/Text';
import Info from '../Screen/Info';
import {
  BOOKINGCAR,
  BROWSEMEMBERS,
  INFO,
  INFOGROUP,
  LEADER,
  MAKEFRIEND,
  MEMBER,
  TEXT1,
} from './ScreenName';
import Leader from '../Screen/Leader';
import BrowseMembers from '../Screen/BrowseMembers';
import Member from '../Screen/Member';
import InfoGroup from '../Screen/InfoGroup';
import MakeFriend from '../Screen/MakeFriend';
import BookingCar from '../Screen/Book/Book';
const Stack = createNativeStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name={INFO} component={Info} />
        <Stack.Screen name={LEADER} component={Leader} />
        <Stack.Screen name={TEXT1} component={Text1223} />
        <Stack.Screen name={BROWSEMEMBERS} component={BrowseMembers} />
        <Stack.Screen name={MEMBER} component={Member} />
        <Stack.Screen name={INFOGROUP} component={InfoGroup} />
        <Stack.Screen name={MAKEFRIEND} component={MakeFriend} />
        <Stack.Screen name={BOOKINGCAR} component={BookingCar} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
