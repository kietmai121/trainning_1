import {responsive, colors} from '../styles';

export const MessageStyle = {
  success: {
    style: {backgroundColor: colors.BLUE},
    duration: 3000,
    titleStyle: {
      fontWeight: 'bold',
      fontSize: 16,
    },
    colors: colors.WHITE,
  },
  success: {
    style: {backgroundColor: colors.GREENLIGHT},
    duration: 3000,
    titleStyle: {
      fontWeight: 'bold',
      fontSize: 16,
    },
    colors: colors.WHITE,
  },
  error: {
    style: {backgroundColor: '#FB7181'},
    duration: 5000,
    titleStyle: {
      fontWeight: 'bold',
      fontSize: 16,
    },
    colors: colors.WHITE,
  },
};
