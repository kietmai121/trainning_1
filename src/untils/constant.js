import {Dimensions} from 'react-native';

export const windows = {
  height: Dimensions.get('window').height,
  width: Dimensions.get('window').width,
};
