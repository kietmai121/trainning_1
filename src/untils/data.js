export const dataTrademark = [
  {
    id: 0,
    select: false,
    lable: '3M',

  },
  {
    id: 1,
    select: false,
    lable: 'Sputter Film',
  },
  {
    id: 2,
    select: false,
    lable: 'Carbon',
  }
]
export const dataCarCompany = [
  {
    id: 0,
    select: false,
    lable: 'Toyota',

  },
  {
    id: 1,
    select: false,
    lable: 'Vifash',
  },
  {
    id: 2,
    select: false,
    lable: 'Kia Morning',
  }
]

export const dataOrgin = [
  {
    id: 0,
    select: false,
    lable: 'Việt nam',

  },
  {
    id: 1,
    select: false,
    lable: 'Trung Quốc',
  },
]
export const dataEvaluate = [
  {
    id: 0,
    select: false,
    lable: '5 sao',

  },
  {
    id: 1,
    select: false,
    lable: 'từ 4 sao',
  },
  {
    id: 2,
    select: false,
    lable: 'từ 3 sao',
  },
  {
    id: 3,
    select: false,
    lable: 'từ 2 sao',
  },
  {
    id: 4,
    select: false,
    lable: 'từ 1 sao',
  },
]

export const dataPromotion = [
  {
    id: 0,
    select: false,
    lable: 'Được trợ giúp',

  },
  {
    id: 1,
    select: false,
    lable: 'Được giảm giá',
  },

]
export const dataListLeader = [
  {
    id: 0,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 1,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 2,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 3,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 4,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 5,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 6,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 7,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  },
  {
    id: 8,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan'
  }

]
export const dataListMemberBrowser = [
  {
    id: 0,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 1,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 2,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 3,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 4,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 5,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 6,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  },
  {
    id: 7,
    images: 'https://ghantee.com/wp-content/uploads/2022/08/most-beautiful-trishul-image-on-the-internet-today-576x1024.jpg',
    name: 'Lan Lan',
    time: '2 phút trước'
  }
]
export const productDetail = {
  images: [
    'https://file.hstatic.net/1000376021/file/men-3_d04781b2a79d4ac1858d5fd94c76bd10.jpg',
    'https://mona.media/wp-content/uploads/2021/06/banner-giay.png',
    'https://snkrvn.com/wp-content/uploads/2018/09/Cole-Banner-01.jpg'
  ],
  nameProduct:'Combo giày-Cole Haan 3.ZERØGRAND – Tương lai là ở đây',
  price:500000,
  preiceUpdate:600000,
  start:2,
  sold:100,
  comment:45,
  Shop:{
    name:'DJKDKLD',
    Address:'Hồ chí minh'
  },
  service:[
    {
      id:1,
      distance:'33 km',
      addRess:'10 quốc lộ 1A, Bình Dương'
    },
    {
      id:2,
      distance:'33 km',
      addRess:'Số 49, đường số 12, khu phố 5, Hiệp bình chánh'
    }
  ],
  serviceDetail:'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incidudunt ut labore et dolore magna aliqua. Ut enim ab minim veniem, qui nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\n Duis aute irure dolor in reprehenderit in voluptate a velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,sunt in culpa qui offcia deserunt mollit anim'
  }