import React from 'react';
import {
  FlatList,
  StyleSheet,
  View,
  Image,
  TextInput,
  ListRenderItem,
  Text
} from 'react-native';
import {Images} from '../../assets';
import Header from '../../custom/Header';
import {responsive} from '../../styles';
import {dataListMemberBrowser} from '../../untils/data';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default function Member() {
  const renderHeader = () => {
    return (
    
        <View style={styles.viewInput}>
          <View style={styles.viewRowJus}>
            <TextInput style={styles.input} placeholder="Tìm kiếm" />
            <View style={styles.viewRow}>
              <FontAwesome
                name={'search'}
                size={20}
                color={'#000'}
                style={styles.iconLeft}
              />
              <Image
                source={Images.Scan}
                style={{width: responsive(20), height: responsive(20)}}
              />
            </View>
          </View>
      
      </View>
    );
  };

  const _renderItem: ListRenderItem<any> = ({item, index}) => {
    return (
      <View style={styles.viewItem}>
        <View style={styles.viewRow}>
        <Image source={{uri: item.images}} style={styles.images} />
        <View style={{marginLeft:responsive(10)}}>
            <Text style={{color:'#000',fontSize:responsive(17)}}>{item.name}</Text>
            <Text style={{marginTop:responsive(5)}}>Trương nhóm</Text>
        </View>
        </View>
       <View style={{alignItems:'flex-end'}}>
              <Text>{item.time}</Text>
              {
                index==0?    <FontAwesome
                name={'key'}
                size={20}
                color={'yellow'}
                style={{marginTop:responsive(5)}}
              />:null
              }
          
       </View>
    
       
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        lable="Xem thành viên"
        iconLeft="angle-left"
        iconRight="user-plus"
      />

      <FlatList
      style={{padding:responsive(15)}}
        ListHeaderComponent={renderHeader}
        data={dataListMemberBrowser}
        renderItem={_renderItem}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor:'#FFF'
  },
  viewInput: {
    backgroundColor: '#f5f5f5',
    borderRadius: responsive(10),
    paddingHorizontal: responsive(10),
  },
  input: {
    width: '75%',
  },
  viewRow: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  viewRowJus: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconLeft: {
    marginRight: responsive(15),
    justifyContent:"flex-end"
  },
  viewItem:{
    flexDirection:'row',
    paddingVertical:responsive(10),
    justifyContent:'space-between',
    alignItems:'center'

  },
  images:{
    width:responsive(70),
    height:responsive(70),
    borderRadius:50,
   
  }
});
