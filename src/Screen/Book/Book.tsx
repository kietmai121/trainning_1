import React, {useState, useCallback, useMemo, useRef} from 'react';
import {productDetail} from '../../untils/data';
import {
  View,
  Text,
  Image,
  FlatList,
  ListRenderItem,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import {ScrollView} from 'react-native-gesture-handler';
import {windows} from '../../untils/constant';
import {responsive} from '../../styles';
import FormatMoneyDot from '../../untils/FormatNumber';
import RatingStar from '../../custom/RatingStar';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Portal} from 'react-native-paper';
import BottomSheet from '../../custom/BottomSheet/BottomSheet';
export default function BookingCar() {
  const [data, setData] = useState(productDetail);
  // ref

  // variables
  const snapPoints = useMemo(() => ['10%', '50%'], []);

  // callbacks


  const _renderIterm: ListRenderItem<any> = ({item, index}) => {
    return (
      <View key={index}>
        <Image
          source={{uri: item}}
          style={styles.imagesBanner}
          resizeMode="stretch"
        />
      </View>
    );
  };
  return (
    <Portal>
      <View style={styles.container}>
        <ScrollView>
          <View style={{backgroundColor: '#FFF'}}>
            <FlatList
              horizontal
              data={data.images}
              renderItem={_renderIterm}
              pagingEnabled
              showsHorizontalScrollIndicator={false}
            />
            <View style={styles.viewRowCenter}>
              {data.images.map((item, index) => (
                <View
                  key={index}
                  style={{
                    marginLeft: index == 0 ? responsive(0) : responsive(10),
                    marginTop: responsive(10),
                  }}>
                  <Image style={styles.imagesDot} source={{uri: item}} />
                </View>
              ))}
            </View>
            <View style={styles.viewText}>
              <Text style={styles.textName}>{data.nameProduct}</Text>
              <View style={styles.viewRow}>
                <Text style={styles.textPrice}>{`${FormatMoneyDot(
                  data.price,
                )}đ`}</Text>
                <Text style={styles.textPriceUpdate}>{`${FormatMoneyDot(
                  data.preiceUpdate,
                )}đ`}</Text>
              </View>
              <View style={styles.viewRow}>
                <View style={styles.viewRowLeftWidth}>
                  <RatingStar rating={data.start} />
                  <Text style={styles.textStart}>{`${data.start}.0`}</Text>
                </View>
                <View style={styles.viewRowLeftWidth}>
                  <Text style={styles.textStart}>{`${data.sold} đã bán`}</Text>
                </View>
                <Text
                  style={
                    styles.textStart
                  }>{`Xem ${data.comment} đánh giá`}</Text>
              </View>
            </View>
          </View>
          <View style={styles.viewBackWhile}>
            <View style={styles.viewRowJus}>
              <Text style={styles.textName}>
                {'Chọn thộc tính cho dịch vụ'}
              </Text>
              <FontAwesome name={'angle-right'} size={25} color={'#000'} />
            </View>
            <Text style={styles.textMaginTop}>Số chỗ ngồi, hãng xe...</Text>
          </View>
          <View style={styles.viewBackWhile}>
            <View style={styles.viewRowJus}>
              <View style={styles.viewRow}>
                <View style={styles.custom}></View>
                <View style={{marginLeft: 10}}>
                  <Text style={styles.textNameShop}>{data.Shop.name}</Text>
                  <Text>{data.Shop.Address}</Text>
                </View>
              </View>
              <View style={styles.customBTN}>
                <Text style={styles.textBtn}>Xem shop</Text>
              </View>
            </View>
          </View>
          <View style={styles.viewBackWhile}>
            <Text style={styles.textName}>Chi nhánh dịch vụ</Text>
            {data.service.map((item, index) => (
              <View
                style={{...styles.viewRow, marginTop: responsive(10)}}
                key={index}>
                <View style={styles.viewCustom}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: '#ffa113',
                    }}>{`${item.distance}`}</Text>
                </View>
                <Text style={{marginLeft: responsive(10), color: '#000'}}>
                  {item.addRess}
                </Text>
              </View>
            ))}
            <View style={{alignItems: 'center'}}>
              <Text>Xem thêm</Text>
            </View>
          </View>
          <View style={styles.viewBackWhile}>
            <Text style={styles.textName}>Chi tiết dịch vụ</Text>
            <Text>{data.serviceDetail}</Text>
          </View>
        </ScrollView>

        <View style={styles.viewBtnWhile}>
          <TouchableOpacity
            style={styles.viewBtn}
            activeOpacity={0.7}
            onPress={() => console.log('sàdsa')}>
            <Text style={styles.textBtn}>Đặt lịch ngay</Text>
          </TouchableOpacity>
        </View>
      </View>
      <BottomSheet />
    </Portal>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#f5f5f5',
  },
  imagesBanner: {
    width: windows.width,
    height: responsive(300),
  },
  imagesDot: {
    width: responsive(30),
    height: responsive(30),
    borderRadius: responsive(10),
  },
  viewRowCenter: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewRowJus: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewText: {
    padding: responsive(15),
  },
  textName: {
    fontSize: responsive(18),
    color: '#000',
    fontWeight: 'bold',
  },
  textPrice: {
    fontSize: responsive(18),
    color: '#ffa113',
    fontWeight: 'bold',
  },
  textPriceUpdate: {
    marginLeft: responsive(10),
    color: '#000',
    fontWeight: 'bold',
  },
  viewRowLeftWidth: {
    flexDirection: 'row',
    borderRightWidth: 2,
    borderColor: '#f5f5f5',
  },
  textStart: {
    color: '#212121',
    paddingHorizontal: responsive(10),
  },
  viewBackWhile: {
    backgroundColor: '#FFF',
    marginTop: responsive(5),
    paddingHorizontal: responsive(15),
    paddingVertical: responsive(10),
  },
  textMaginTop: {
    marginTop: responsive(5),
  },
  custom: {
    borderWidth: 1,
    borderColor: '#000',
    width: responsive(50),
    height: responsive(50),
    borderRadius: 10,
  },
  customBTN: {
    width: responsive(120),
    height: responsive(45),
    borderRadius: responsive(10),
    backgroundColor: '#ffa113',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textNameShop: {
    color: '#000',
    fontWeight: 'bold',
  },
  textBtn: {
    color: '#FFF',
    fontSize: responsive(15),
  },
  viewCustom: {
    width: responsive(60),
    height: responsive(25),
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fae6c8',
  },
  viewBtnWhile: {
    backgroundColor: '#FFF',
    height: responsive(70),
    width: '100%',
    paddingHorizontal: responsive(15),
    justifyContent: 'center',
  },
  viewBtn: {
    height: responsive(50),
    backgroundColor: '#ffa113',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: responsive(10),
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 0},
  },
  contentContainerds: {
    flex: 1,
  },
});
