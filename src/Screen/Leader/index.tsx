import React from 'react-native';

import {View, Text, TextInput,Image,FlatList} from 'react-native';
import Header from '../../custom/Header';
import {responsive} from '../../styles';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Images } from '../../assets';
import { dataListLeader } from '../../untils/data';
import ItemLeader from './ItemLeader';
import { useNavigation } from '@react-navigation/native';
export default function Leader() {
  const navigation=useNavigation()
  return (
    <View style={styles.container}>
      <Header
        lable="Chuyển quyền trưởng nhóm"
        iconLeft="angle-left"
        iconRight=""
      />
      <View style={{paddingHorizontal: responsive(15)}}>
        <Text>
          người được chọn sẻ trở thành trưởng nhóm va có mọi quyền quản lý nhóm.
          Bạn sẽ mất quyền quản lý nhóm nhưng vẫn là thành vien của nhóm
        </Text>
        <Text
          style={{
            color: '#000',
            fontSize: responsive(15),
            fontWeight: 'bold',
            paddingVertical: responsive(10),
          }}>
          chọn trưởng nhóm mới
        </Text>
        <View style={styles.viewInput}>
          <View style={styles.viewRowJus}>
            <TextInput style={styles.input} placeholder="Tìm kiếm" />
            <View style={styles.viewRow}>
            <FontAwesome name={'search'} size={20} color={'#000'} style={styles.iconLeft} />
            <Image source={Images.Scan} style={{width:responsive(20),height:responsive(20)}}/>
            </View>
          </View>
        </View>
        
      </View>
    
      <FlatList
        style={{paddingHorizontal:responsive(15)}}
        showsVerticalScrollIndicator={false}
         data={dataListLeader}
         renderItem={({item})=><ItemLeader item={item}/>}
        />
    </View>
  );
}
