import { StyleSheet } from 'react-native';
import { responsive } from '../../../styles';
const styles= StyleSheet.create({
    images:{
        width:responsive(60),
        height:responsive(60),
        borderRadius:responsive(50)
    },
    viewRow:{
        flexDirection:'row',
        marginTop:responsive(15),
        alignItems:"center",
    },
    text:{
        marginLeft:responsive(10),
        color:'#252b31'

    }

})
export default styles