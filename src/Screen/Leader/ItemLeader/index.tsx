import React from 'react-native';
import {View, Text, Image} from 'react-native';
import styles from './styles';
import Swipeable from 'react-native-gesture-handler/Swipeable';

interface type {
  item: any;
}
 const renderLeft=()=>{
   return(
    <View>
      <Text></Text>
    </View>
   )
 }
export default function ItemLeader({item}: any) {
  console.log(item);
  return (
    <Swipeable renderLeftActions={renderLeft}>
      <View style={styles.viewRow}>
        <Image source={{uri: item.images}} style={styles.images} />
        <Text style={styles.text}>{item.name}</Text>
      </View>
    </Swipeable>
  );
}
