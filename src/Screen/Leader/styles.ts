import { responsive } from './../../styles/responsive';
import { StyleSheet } from 'react-native';
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFF'
    },
    viewInput:{
        backgroundColor:"#f5f5f5",
        borderRadius:responsive(10),
        paddingHorizontal:responsive(10)
    },
    input:{
        width:'75%',
    },
    viewRow:{
        alignItems:'center',
        flexDirection:'row',

    },
    viewRowJus:{
        alignItems:'center',
        flexDirection:'row',
        justifyContent:"space-between"
    },
    iconLeft:{
        marginRight:responsive(15)
    }
})
export default styles