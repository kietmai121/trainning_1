import React from 'react';
import {View, Text, StyleSheet, FlatList, Image} from 'react-native';
import Header from '../../custom/Header';
import {responsive} from '../../styles';
import {dataListMemberBrowser} from '../../untils/data';
export default function MakeFriend() {
  const _renderItem = ({item}: any) => {
    console.log(item);
    return (
    
      <View style={styles.viewItem}>
        <View style={styles.viewRow}>
          <Image source={{uri: item.images}} style={styles.images} />
          <View style={{marginLeft: responsive(10)}}>
            <Text style={{color:"#000"}}>{item.name}</Text>
            <Text style={{marginTop: responsive(5)}}>{item.time}</Text>
          </View>
        </View>
        <View style={styles.viewBtnItem}>
            <Text style={styles.textBtn}>
                Thu hồi
            </Text>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header
        lable="Lời mời kết bạn đã gửi"
        iconLeft="angle-left"
        iconRight=""
      />
      <View style={{paddingHorizontal: responsive(15)}}>
        <Text
          style={
            styles.viewTitel
          }>{`${dataListMemberBrowser.length} lời mời kết bạn đã gữi đi`}</Text>
      </View>
      <FlatList
        style={{paddingHorizontal: responsive(15)}}
        data={dataListMemberBrowser}
        renderItem={_renderItem}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    flex: 1,
    backgroundColor: '#FFF',
  },
  viewTitel: {
    fontSize: responsive(16),
    color: '#000',
    fontWeight: 'bold',
  },
  viewItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: responsive(15),
    justifyContent:"space-between"
  },
  images: {
    width: responsive(70),
    height: responsive(70),
    borderRadius: 50,
  },
  viewBtnItem: {
    width: responsive(125),
    height: responsive(50),
    backgroundColor: '#ffeac9',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
  },
  viewRow:{
    flexDirection:'row',
    alignItems:"center"
  },
  textBtn:{
    color:'#ffa113'
}
});
