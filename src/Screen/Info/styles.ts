import { StyleSheet } from 'react-native';
import { responsive } from '../../styles';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5'
    },
    viewAvatar: {

        backgroundColor: "#ffffff"
    },
    customCircle: {
        width: responsive(100),
        height: responsive(100),
        backgroundColor: "red"
    },
    textName: {
        fontSize: responsive(16),
        color: '#414141',
        fontWeight: 'bold'
    },
    ViewImages: {
        marginTop: responsive(5),
        backgroundColor: "#FFF",
        padding: responsive(15)

    },
    viewRowJus: {
        flexDirection: 'row',
        justifyContent: "space-between"
    },
    textTitelImages: {
        color: '#000',
        fontSize: responsive(17),

    },

    textLink: {
        color: '#002de9',
        fontSize: responsive(15)
    },
    viewRow: {
        flexDirection: 'row',
       
    },
    images: {
        width: responsive(80), 
        height: responsive(80),
         borderRadius: responsive(10)
    },
    viewInfo:{
        marginTop: responsive(5),
        backgroundColor:'#FFF',
        padding:responsive(15)
    }
})
export default styles