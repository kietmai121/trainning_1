import React, {useState} from 'react';
import {View, Text, StatusBar, Image, ScrollView} from 'react-native';
import Header from '../../custom/Header';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import InfoBtn from '../../custom/InfoBtn';
import {responsive} from '../../styles';
import {Images} from '../../assets';
import InfoDetail from '../../custom/InfoDetail';
import {useNavigation} from '@react-navigation/native';
import {BOOKINGCAR, BROWSEMEMBERS, INFOGROUP, LEADER, MAKEFRIEND, MEMBER, TEXT1} from '../../router/ScreenName';

export default function Info() {
  const navigation = useNavigation();
  const [data, setData] = useState([
    'https://www.bugatti.com/fileadmin/_processed_/sei/p1/se-image-3bbeac5006e9b894545a3519cc23a735.jpg',
    'https://image.cnbcfm.com/api/v1/image/105775023-1551784325731capture.png?v=1553189757&w=750&h=422&vtcrop=y',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTM1Aq5za-l945ek-VPgewAiUHx1J03Ewv2duCVnk3Hbc90T4JWOHt9wDR6ZMKYhlS7CYE&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRD86k8ZxWNSwX-J9Ks3RaNC2Lt5H6OakPuiw&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT_qc6pXJLJU_0Yurux_9AHJqSem3pbF57hQQ&usqp=CAU',
  ]);

  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={'transparent'}
        translucent={true}
      />
      {/* <FontAwesomeIcon icon="far fa-user-circle" /> */}
      <Header lable="111" iconLeft="angle-left" iconRight="ellipsis-v" />
      <ScrollView>
        <View style={styles.viewAvatar}>
          <View style={{alignItems: 'center'}}>
            <FontAwesome name="user-circle" size={100} color={'#000'} />
            <View style={styles.viewRow}>
              <Text style={styles.textName}>Anh em truc 6</Text>
              <FontAwesome
                name="pencil-square"
                size={25}
                color={'#000'}
                style={{marginLeft: responsive(10)}}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              padding: responsive(15),
              justifyContent: 'space-between',
            }}>
            <InfoBtn
              images={Images.Book}
              lable={'Đến'}
              lable1={'cộng đồng'}
              icon="search"
            />
            <InfoBtn
              images={Images.UserAdd}
              lable={'Thêm'}
              lable1={'Thành viên'}
              icon="search"
            />
            <InfoBtn
              images={''}
              icon="search"
              lable={'Tìm'}
              lable1={'tin nhắn'}
            />
            <InfoBtn
              images={Images.Bell}
              lable={'Tắc'}
              lable1={'thông báo'}
              icon="search"
            />
          </View>
        </View>
        <View style={styles.ViewImages}>
          <View style={styles.viewRowJus}>
            <Text style={styles.textTitelImages}>Đã chia sẻ (9)</Text>
            <Text style={styles.textLink}>Xem tất cả</Text>
          </View>
          <Text style={{color: '#000', paddingVertical: responsive(5)}}>
            Ảnh link đã chia sẻ
          </Text>
          <ScrollView
            style={styles.viewRow}
            horizontal
            showsHorizontalScrollIndicator={false}>
            {data.map((item, index) => {
              return (
                <Image
                  key={index}
                  source={{uri: item}}
                  style={{
                    ...styles.images,
                    marginLeft: index == 0 ? responsive(0) : responsive(10),
                  }}
                  resizeMode={'contain'}
                />
              );
            })}
          </ScrollView>
        </View>
        <View style={styles.viewInfo}>
          <InfoDetail
            onPress={() => navigation.navigate(INFOGROUP)}
            iconLeft="info-circle"
            lable="thông tin nhóm"
            iconRight="angle-right"
            colorIconLeft="#0048d8"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => navigation.navigate(MAKEFRIEND)}
            iconLeft="microphone"
            lable="tự động phát loại đoạn ghi âm"
            iconRight=""
            colorIconLeft="#49945b"
            switchFalse={1}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => console.log('112121')}
            iconLeft="lock"
            lable="trạng thái nhóm"
            iconRight="angle-right"
            colorIconLeft="#fca207"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => console.log('112121')}
            iconLeft="gear"
            lable="cài đặt nhóm"
            iconRight=""
            colorIconLeft="#182c61"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() =>navigation.navigate(MEMBER)}
            iconLeft="users"
            lable="xem thành viên"
            iconRight=""
            colorIconLeft="#fd7272"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() =>navigation.navigate(BROWSEMEMBERS)}
            iconLeft="user-plus"
            lable="phê duyệt thành viên"
            iconRight=""
            colorIconLeft="#fca207"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() =>  navigation.navigate(BOOKINGCAR)}
            iconLeft="trash"
            lable="xóa lịch sử trò chuyện"
            iconRight=""
            colorIconLeft="#ec4040"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => navigation.navigate(LEADER)}
            iconLeft="key"
            lable="quản trị nhóm"
            iconRight=""
            colorIconLeft="#0048d8"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => console.log('112121')}
            iconLeft="warning"
            lable="báo cáo"
            iconRight=""
            colorIconLeft="#27337d"
            switchFalse={0}
            colorRed={0}
          />
          <InfoDetail
            onPress={() => navigation.navigate(TEXT1)}
            iconLeft="sign-out"
            lable="rời khỏi nhóm"
            iconRight=""
            colorIconLeft="#bc0000"
            switchFalse={0}
            colorRed={1}
          />
        </View>
      </ScrollView>
    </View>
  );
}
