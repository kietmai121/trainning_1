import React ,{useState,useCallback}from 'react'

import {View,Text,TouchableOpacity} from 'react-native'
import { responsive } from '../../styles'
import Custom from './Custom'

export default function ExampleHook(){
    const [number,setNumber]=useState<number>(0)
    
    const handelSubmid=useCallback(()=>{
        setNumber(count=>count+1)
    },[])

     return(
        <View style={{flex:1,alignItems:'center',justifyContent:"center",width:'100%',height:'100%'}}>
            <Text>{number}</Text>
            <Custom onPress={handelSubmid}/>
        </View>
     )
}