import { responsive } from './../../styles/responsive';
import { colors } from './../../styles/colors';
import { StyleSheet } from "react-native";

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:colors.WHITE
    },
    header:{
        height:responsive(60),
        width:'100%',
        alignItems:"flex-end",
        paddingHorizontal:responsive(15),
        flexDirection:"row"
    },
    textHeader:{
        fontWeight:'bold',
        fontSize:responsive(17),
        color:colors.BLACK
    },
    textHeader1:{
        fontWeight:'bold',
        fontSize:responsive(17),
        color:colors.BLACK,
    },
    body:{
        padding:responsive(15)
    },
    custom:{
        backgroundColor:'rgb(245,245,245)',
        borderRadius:10,
        marginTop:responsive(10)

    },
    customWith:{
        backgroundColor:'rgb(245,245,245)',
        borderRadius:10,
        width:'48%',
        alignItems:"center",
        justifyContent:"center",
        marginTop:responsive(10)

    },
    viewRow:{
        flexDirection:"row",
    },
    viewRowJus:{
        flexDirection:"row",
        flexWrap: 'wrap',
        justifyContent:"space-between"

    },
    textTitel:{
        marginTop:responsive(15),
        fontWeight:'bold',
        fontSize:responsive(17),
        color:colors.BLACK,
      
    },
    textKm:{
        color:'#000',
        fontSize:15,
    },
    viewBottom:{
        paddingHorizontal:15,
        flexDirection:'row',
        alignItems:"center",
        justifyContent:"space-between",
        height:responsive(80)
    },
    viewBTN:{
        backgroundColor:'rgba(255,192,96, 0.16)',
        borderRadius:10,
        height:responsive(50),
        width:'48%',
        alignItems:"center",
        justifyContent:"center"
    },
    thumb:{
        backgroundColor: 'rgb(255,161,19)',
        borderRadius: 30 / 2,
        height: 20,
        shadowColor: 'blue',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.35,
        shadowRadius: 2,
        width: 20,
    },
    track: {
        borderRadius: 10,
        height: 5,
    },

})
export default styles