import React from 'react';
import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {responsive} from '../../styles';
import {windows} from '../../untils/constant';
import {Slider} from '@miblanchard/react-native-slider';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import HookText from './Hook';
import styles from './styles';
import InputRage from '../../custom/InputRage';
import Header from '../../custom/Header';
export default function Text1223() {
  const {
    check,
    data,
    checkCar,
    dataCar,
    dataO,
    checkOR,
    dataE,
    checkRage,
    checkP,
    dataP,
    onCleanCheck,
    setSlideCompletionCount,
    slideCompletionCount,
    numberStart,
    setNumberStart
  } = HookText();
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle={'dark-content'}
        backgroundColor={'transparent'}
        translucent={true}
      />
   <Header
        lable="dsdsds"
        iconLeft="angle-left"
        iconRight=""
      />
      <ScrollView style={styles.body}>
        <Text style={styles.textHeader1}>Thương hiệu</Text>
        <View style={styles.viewRow}>
          {data.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => check(item.id)}
                key={index}
                style={{
                  ...styles.custom,
                  marginLeft: index == 0 ? responsive(1) : responsive(10),
                  borderWidth: item?.select ? 1 : 0,
                  borderColor: item?.select ? '#FF6600' : '#FFF',
                }}
                activeOpacity={0.7}>
                <Text
                  style={{
                    padding: responsive(15),
                    color: item?.select == true ? '#FF6600' : '#000',
                  }}>
                  {item.lable}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>

        <Text style={styles.textTitel}>Hãng xe</Text>
        <View style={styles.viewRow}>
          {dataCar.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => checkCar(item.id)}
                key={index}
                style={{
                  ...styles.custom,
                  marginLeft: index == 0 ? responsive(1) : responsive(10),
                  borderWidth: item?.select == true ? 1 : 0,
                  borderColor: item?.select == true ? '#FF6600' : '#FFF',
                }}
                activeOpacity={0.7}>
                <Text
                  style={{
                    padding: responsive(15),
                    color: item?.select == true ? '#FF6600' : '#000',
                  }}>
                  {item.lable}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        <Text style={styles.textTitel}>Xuất xứ</Text>
        <View style={styles.viewRowJus}>
          {dataO.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => checkOR(item.id)}
                key={index}
                style={{
                  ...styles.customWith,
                  marginLeft: index == 0 ? responsive(1) : responsive(10),
                  borderWidth: item?.select ? 2 : 0,
                  borderColor: item?.select ? '#FF6600' : '#FFF',
                }}
                activeOpacity={0.7}>
                <Text
                  style={{
                    padding: responsive(15),
                    color: item?.select == true ? '#FF6600' : '#000',
                  }}>
                  {item.lable}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        <Text style={styles.textTitel}>Đánh giá</Text>

        <View style={styles.viewRowJus}>
          {dataE.map((item, index) => {
            return (
              <TouchableOpacity
                onPress={() => checkRage(item.id)}
                key={index}
                style={{
                  ...styles.customWith,
                  borderWidth: item?.select ? 1 : 0,
                  borderColor: item?.select ? '#FF6600' : '#FFF',
                }}
                activeOpacity={0.7}>
                <Text
                  style={{
                    padding: responsive(15),
                    color: item?.select == true ? '#FF6600' : '#000',
                  }}>
                  {item.lable}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>

        <Text style={styles.textTitel}>Khuyến mãi</Text>
        <View style={{width: '100%'}}>
          <View style={styles.viewRowJus}>
            {dataP.map((item, index) => {
              return (
                <TouchableOpacity
                  onPress={() => checkP(item.id)}
                  key={index}
                  style={{
                    ...styles.customWith,
                    marginLeft: index == 0 ? responsive(1) : responsive(10),
                    borderWidth: item?.select == true ? 1 : 0,
                    borderColor: item?.select == true ? '#FF6600' : '#FFF',
                  }}
                  activeOpacity={0.7}>
                  <Text
                    style={{
                      padding: responsive(15),
                      color: item?.select == true ? '#FF6600' : '#000',
                    }}>
                    {item.lable}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
        
          <Text style={styles.textTitel}>Khoản cách</Text>
          {/* <Text>{slideCompletionCount}</Text> */}
           {/* <InputRage max={40} min={0} onChangeMax={()=>console.log('12')} onChangeMin={()=>console.log('123')}/> */}
           <Slider
            // style={{width: '100%', height: 30}}
            minimumValue={10}
            maximumValue={40}
            step={10}
            value={slideCompletionCount}
            onValueChange={value=>setSlideCompletionCount(value)}
            minimumTrackTintColor='rgb(255,161,19)'
            maximumTrackTintColor='#000'
            thumbTintColor='rgb(255,161,19)'            
            thumbStyle={styles.thumb}
            trackStyle={styles.track}
         
            // onTouchStart={(cd)=>console.log(cd)}
           />
           <View style={{width:"100%",flexDirection:"row",justifyContent:"space-between",height:responsive(30)}}>
             <Text style={styles.textKm}>{10+ 'KM'}</Text>
             <Text style={styles.textKm}>{slideCompletionCount+'KM'}</Text>
           </View>
      </ScrollView>

      <View style={styles.viewBottom}>
        <TouchableOpacity style={styles.viewBTN} onPress={() => onCleanCheck()}>
          <Text style={{color: '#FF6600'}}>Thiết lập lại</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{...styles.viewBTN, backgroundColor: 'rgb(255,161,19)'}}>
          <Text style={{color: '#FFF'}}>Áp dụng</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
