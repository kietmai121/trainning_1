import {useState} from 'react';
import {
  dataCarCompany,
  dataEvaluate,
  dataOrgin,
  dataPromotion,
  dataTrademark,
} from '../../untils/data';

const HookText = () => {
  const [data, setData] = useState(dataTrademark);
  const [slideCompletionCount, setSlideCompletionCount] = useState<number|number[]>(10);
  const [dataCar, setDataCar] = useState(dataCarCompany);
  const [dataO, setDataO] = useState(dataOrgin);
  const [dataE, setDataE] = useState(dataEvaluate);
  const [dataP,setDataP]=useState(dataPromotion)
  const [numberStart,setNumberStart]=useState(0)
  const check = (id: number) => {
    console.log(id);
    const dataNew = data.map(item => {
      if (item.id == id) {
        return {...item, select: !item.select};
      }
      return {...item, select: false};
    });
    console.log(dataNew)
    setData(dataNew);
  };
  const checkCar = (id: number) => {
    const dataNew = dataCar.map(item => {
      if (item.id == id) {
        return {...item, select: !item.select};
      }
      return {...item};
    });
    setDataCar(dataNew);
  };
  const checkOR = (id: number) => {
    console.log(id);
    const dataNew = dataO.map(item => {
      if (item.id == id) {
        return {...item, select: !item.select};
      }
      return {...item, select: false};
    });

    setDataO(dataNew);
  };
  const checkRage = (id: number) => {
    console.log(id);
    const dataNew = dataE.map(item => {
      if (item.id == id) {
        return {...item, select: !item.select};
      }
      return {...item, select: false};
    });

    setDataE(dataNew);
  };
  const checkP = (id: number) => {
    console.log(id);
    const dataNew = dataP.map(item => {
      if (item.id == id) {
        return {...item, select: !item.select};
      }
      return {...item, select: false};
    });

    setDataP(dataNew);
  };
  const onCleanCheck=()=>{
    setData(dataTrademark)
    setDataCar(dataCarCompany)
    setDataO(dataOrgin)
    setDataE(dataEvaluate)
    setDataP(dataPromotion)
  }
  return {
    check,
    data,
    dataCar,
    checkCar,
    dataO,
    checkOR,
    checkRage,
    dataE,
    checkP,
    dataP,
    onCleanCheck,
    slideCompletionCount,
    setSlideCompletionCount,
    numberStart,
    setNumberStart

  };
};
export default HookText;
