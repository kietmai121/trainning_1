import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Header from '../../custom/Header';
import { responsive } from '../../styles';

export default function InfoGroup() {
  return (
    <View style={styles.container}>
      <Header lable="Thông tin nhóm" iconLeft="angle-left" iconRight="" />
      <View style={{paddingHorizontal:responsive(15)}}>
      <Text>
        {
          'Nhóm được tạo ra nhằm mục đích giúp đỡ, trao đổi thông tin, mua bán hàng hóa, hỗ trợ nhau tìm dược nguồn hàng với giá cả hợp lý và chất lượng \n\n yêu cầu không spam và dùng từ nghữ thô tục. không đăng bài trái quy định của nhà nước và pháp luật\n\n bài đăng sẻ được kiểm duyệt trước khi hiển thị công khai. Vui lòng đăng bài theo quy định của nhóm'
        }
      </Text>
      <View >

</View>
      </View>
     
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    flex: 1,
    backgroundColor: '#FFF',
  },
});
