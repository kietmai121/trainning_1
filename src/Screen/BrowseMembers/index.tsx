import React from 'react'
import {View,FlatList, StyleSheet} from 'react-native'
import Header from '../../custom/Header'
import { responsive } from '../../styles'
import { dataListMemberBrowser } from '../../untils/data'
import ItemListBrowsemember from './ItemList'

export  default function BrowseMembers(){
    return (
        <View style={styles.container}>
              <Header lable="Duyệt thành viên" iconLeft="angle-left" iconRight="" />
              <FlatList
               style={{paddingHorizontal:responsive(15)}}
                data={dataListMemberBrowser}
                renderItem={({item})=><ItemListBrowsemember item={item}/>}
              />
        </View>
    )
}

const styles=StyleSheet.create({
  container:{
     flex:1,
     width:"100%",
     height:'100%',
     backgroundColor:'#FFF'
  }
})