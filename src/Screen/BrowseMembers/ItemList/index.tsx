import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {responsive} from '../../../styles';
import styles from './styles';

interface type {
  item: any;
}

export default function ItemListBrowsemember({item}: type) {
  console.log(item);
  return (
    <View>
      <View style={styles.viewRow}>
        <Image
          source={{uri: item.images}}
          style={{
            width: responsive(70),
            height: responsive(70),
            borderRadius: responsive(40),
          }}
        />
        <View style={{paddingHorizontal:responsive(15)}}>
            <View style={{paddingVertical:responsive(15)}}>
            <Text style={{color:'#000'}}>{item.name}</Text>
          <Text>{item.time}</Text>
        
            </View>
        <View style={styles.viewRowJus}>
          <TouchableOpacity style={styles.viewBtn}>
            <Text style={styles.textBtn}>Xóa</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{...styles.viewBtn, backgroundColor: '#ffa113',marginLeft:responsive(10)}}>
            <Text style={{color: '#FFF'}}>Chấp nhận</Text>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    </View>
  );
}
