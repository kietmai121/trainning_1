import React from "react";
import { StyleSheet } from "react-native";
import { responsive } from "../../../styles";

const styles=StyleSheet.create({
    viewRow:{
       flexDirection:"row",  
       alignItems:'center'  
    
    },
    viewRowJus:{
       
        flexDirection:"row",  
        alignItems:'center',
        justifyContent:'space-between',
 
    },
    viewBtn:{
        width:responsive(125),
        height:responsive(45),
        backgroundColor:'#ffeac9',
        alignItems:'center',
        justifyContent:'center',
        borderRadius:10

    },
    textBtn:{
        color:'#ffa113'
    }
})
export default styles