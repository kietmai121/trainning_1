import { colors } from './../../styles/colors';
import { StyleSheet } from 'react-native';
import { responsive } from '../../styles';
const styles=StyleSheet.create({
    container:{
        height:responsive(60),
        width:'100%',
        alignItems:"flex-end",
        justifyContent:"space-between",
        paddingHorizontal:responsive(15),
        flexDirection:"row",
        backgroundColor:'#FFF'
    },
    textHeader:{
        fontWeight:'bold',
        fontSize:responsive(17),
        color:colors.BLACK,
        marginLeft:responsive(10)
    },
    viewRow:{
        flexDirection:'row'
    }
})
export default styles