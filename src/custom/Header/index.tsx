import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';
interface type {
  iconRight: string;
  iconLeft: string;
  lable: string;
}

export default function Header({iconLeft, iconRight, lable}: type) {
  const navigation=useNavigation()
  return (
    <View style={styles.container}>
      <View style={styles.viewRow}>
      <FontAwesome name={iconLeft} size={25}  color={'#000'} onPress={()=>navigation.goBack()}/>
        <Text style={styles.textHeader} >{lable}</Text>
      </View>
      
        {iconRight==''?null:<FontAwesome name={iconRight} size={25} color={'#000'} />}
      
      
    </View>
  );
}
