import React, { useState } from "react";
import {
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Icons } from "../../../images/icons";
import styles from "./styles";
import AntDesign from "react-native-vector-icons/AntDesign";


const RatingStar = ({ rating }) => {
  const stars = [0, 1, 2, 3, 4];
  const onRate = (star) => {

  };
  return (
    <View style={styles.container}>
      {stars.map((star) => (
  
          <AntDesign
          key={star}
            style={styles.star}
            name="star"
            size={14}
             color={star > rating ? '#000' : '#ffa113'}
          />
      ))}
    </View>
  );
};

export default RatingStar;
