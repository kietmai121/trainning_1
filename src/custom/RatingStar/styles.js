import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignSelf: "center",
  },
  star: {
    margin: 1,
   tintColor:"red"
  },
  rated: {
    // backgroundColor: "red",
  },
});

export default styles;
