import { responsive } from './../../styles/responsive';
import { StyleSheet } from 'react-native';
const styles=StyleSheet.create({
    
    viewRowJus:{
        height:responsive(50),
        borderBottomWidth:1,
        borderColor:'#d9d9d9',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'

    },
    viewRow:{
        flexDirection:'row',
        alignItems:'center'
    },
    textTitel:{
        color:'#000',
        marginLeft:responsive(10)
    }
})
export default styles