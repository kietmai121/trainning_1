import React, {useState} from 'react';
import {View, Text, Switch, TouchableOpacity} from 'react-native';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
interface type {
  iconLeft: string;
  iconRight: string;
  lable: string;
  colorIconLeft: string;
  switchFalse: number;
  colorRed: number;
  onPress:()=>void
}

export default function InfoDetail({
  iconLeft,
  iconRight,
  lable,
  switchFalse,
  colorIconLeft,
  colorRed,
  onPress
}: type) {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  return (
    <TouchableOpacity style={styles.viewRowJus} activeOpacity={0.7} onPress={()=>onPress()}>
      <View style={styles.viewRow}>
        <FontAwesome name={iconLeft} size={25} color={colorIconLeft} />
        <Text
          style={{
            ...styles.textTitel,
            color: colorRed == 1 ? '#bc0000' : '#000',
          }}>
          {lable}
        </Text>
      </View>
      {iconRight == '' ? (
        switchFalse == 1 ? (
          <Switch
            trackColor={{false: '#000', true: 'rgba(255,192,96, 0.16)'}}
            thumbColor={isEnabled ? '#fca207' : '#ffa113'}
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        ) : null
      ) : (
        <FontAwesome name={iconRight} size={25} color={'#000'} />
      )}
    </TouchableOpacity>
  );
}
