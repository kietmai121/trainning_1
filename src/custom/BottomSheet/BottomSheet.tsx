import React, {useRef} from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import {Portal} from 'react-native-paper';
import Animated from 'react-native-reanimated';

export default function BottomSheet() {
  const botomSheeetHeight = Dimensions.get('window').height;
  const bottomSheetWidth = Dimensions.get('window').width;
//   const bottom=useRef(new Animated.value(-botomSheeetHeight)).current

  return (
    <Portal>
      <Animated.View
        style={{
          ...styles.container,
          height: botomSheeetHeight,
          bottom:0
        }}></Animated.View>
    </Portal>
  );
}
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 100,
    backgroundColor: '#FFF',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    shadowColor:'#000',
    shadowOffset:{
        height:-3,
        width:0
    },
    shadowOpacity:0.24,
    shadowRadius:4
  },
});
