import { responsive } from './../../styles/responsive';
import { StyleSheet } from 'react-native';
const styles=StyleSheet.create({
    viewBtn:{
       width:'23%',
       height:responsive(120),
       backgroundColor:'#f5f5f5',
      borderRadius:responsive(10),
      alignItems:'center',
      justifyContent:"center"
    },
    viewImage:{
        width:responsive(30),
        height:responsive(30)
    },
    text:{
        color:'#000000',
        fontSize:responsive(16),
        marginTop:responsive(10)
    },
    text1:{
        color:'#000000',
        fontSize:responsive(16),
        marginTop:responsive(2)
    }
})
export default styles