import React from 'react';
import {View, Image, Text} from 'react-native';
import {responsive} from '../../styles';
import styles from './styles';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
interface type {
  images: any;
  lable: string;
  lable1: string;
  icon:string
}
export default function InfoBtn({images, lable,lable1,icon}: type) {
  return (
    <View style={styles.viewBtn}>
      {images==''?<FontAwesome name={icon} size={25}/>:<Image source={images} style={styles.viewImage} />
  
}
        <Text style={styles.text}>{lable}</Text>
        <Text style={styles.text1}>{lable1}</Text>
      
    </View>
  );
}
